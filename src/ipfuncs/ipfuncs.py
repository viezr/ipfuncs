'''
Module with some useful functions for IPv4 addresses.
'''
import re


_NTOA_MAX = 4294967295 # Maximum input value for ntoa conversion.
_IPV4_PATTERN = re.compile(
    "[0-9]{1,3}[.][0-9]{1,3}[.][0-9]{1,3}[.][0-9]{1,3}")
_IPV4PORT_PATTERN = re.compile(
    "[0-9]{1,3}[.][0-9]{1,3}[.][0-9]{1,3}[.][0-9]{1,3}[:][0-9]{2,6}")

def is_ip(ip_addr: str) -> bool:
    '''
    Check if string is ip address.
    '''
    _check_string(ip_addr)
    return _ip_correct(ip_addr)

def get_ip(ip_addr: str) -> list:
    '''
    Get all ip addresses from string, and return list of found ip addresses.
    '''
    _check_string(ip_addr)
    ip_list = [ip for ip in _IPV4_PATTERN.findall(ip_addr) if _ip_correct(ip)]
    return ip_list

def get_ip_port(ip_addr: str) -> list:
    '''
    Get all ip-address:port substrings from string, and return as list.
    '''
    _check_string(ip_addr)
    ip_list = [ip for ip in _IPV4PORT_PATTERN.findall(ip_addr)
        if _ip_correct(ip.split(":")[0])]
    return ip_list

def aton(ip_addr: str) -> int:
    '''
    Convert IPv4 dotted to decimal format.
    '''
    _check_string(ip_addr)
    if not _ip_correct(ip_addr):
        raise ValueError("Wrong IPv4 address.")
    num1, num2, num3, num4 = (int(x) for x in ip_addr.split("."))
    ip_dec = (num1 << 24) + (num2 << 16) + (num3 << 8) + num4
    return ip_dec

def ntoa(num: int) -> str:
    '''
    Convert decimal format to IPv4 dotted string format.
    '''
    if not isinstance(num, int):
        err = f"Expected integer argument. Got: {type(num)}"
        raise TypeError(err)
    if not 0 <= num <= _NTOA_MAX:
        raise ValueError("Decimal IPv4 address is out of range.")
    nums = (num >> 24, 255 & (num >> 16), 255 & (num >> 8), 255 & num)
    ip_addr = '.'.join(str(int(x)) for x in nums)
    return ip_addr

def _get_ip(ip_addr: str, pattern: str) -> list:
    ip_find = re.compile(pattern)
    return ip_list

def _check_string(value: str) -> None:
    '''
    Check if provided value is a string.
    '''
    if not isinstance(value, str):
        err = f"Expected string argument. Got: {type(value)}"
        raise TypeError(err)

def _ip_correct(ip_addr: str) -> bool:
    '''
    Checking ip structure and range of numbers.
    '''
    if " " in ip_addr:
        return False
    ip_nums = ip_addr.split(".")
    if len(ip_nums) != 4 or not _ip_in_ranges(ip_nums):
        return False
    return True

def _ip_in_ranges(ip_nums: list) -> bool:
    '''
    Check that numbers in ip address are correct.
    '''
    for num in ip_nums:
        try:
            num = int(num)
        except ValueError:
            return False
        if not _num_in_range(num):
            return False
    return True

def _num_in_range(num: int) -> bool:
    '''
    Check if string is a number in range 0 <= n <= 255.
    '''
    if 0 <= int(num) <= 255:
        return True
    return False
