'''
Unit tests for check ip module.
'''
from sys import path
import unittest

path.insert(0, "..")
from src.ipfuncs import is_ip, get_ip, get_ip_port, aton, ntoa
from src.ipfuncs.ipfuncs import _ip_in_ranges, _num_in_range, _NTOA_MAX


class CheckIpTests(unittest.TestCase):
    def run(self, result=None):
        '''
        Stop after first error
        '''
        if not result.errors:
            super(self.__class__, self).run(result)

    @classmethod
    def setUpClass(cls):
        cls.wrong_ip_list = [
            "1.2.168.1.1",
            "1,2.168.1.1",
            "1192.168.1.1",
            "d92.168.1.1",
            "192.168.1.d",
            "255.255.256.255",
            "-1.255.255.255",
            "1.0.0.1000",
            " 1.0.0.0",
        ]
        cls.correct_ip_list = [
            "192.168.1.1",
            "1.2.168.1",
            "0.0.0.0",
            "1.0.0.0",
            "255.255.255.255",
        ]
        cls.str_decimal_list = [
            ("0.0.0.0", 0),
            ("0.0.0.1", 1),
            ("0.0.1.0", 256),
            ("192.168.10.2", 3232238082),
            ("255.255.255.255", _NTOA_MAX),
        ]

    def setUp(self):
        pass

    def tearDown(self):
        pass

    @classmethod
    def tearDownClass(cls):
        pass

    def test_nonStringValue_returnTypeError(self):
        ip = 133
        self.assertRaises(TypeError, is_ip, ip)

    def test_wrongStrNum_returnFalse(self):
        ip_list = [["d92", "168", "1", "1"], ["192", "168", "1", "r"]]
        with self.subTest():
            for ip in ip_list:
                result = _ip_in_ranges(ip)
                self.assertFalse(result)

    def test_getMulitpleIp_returnCorrectNumIp(self):
        ip_list = [
            [0, "1sd.2.168.1 skljdf 119r.168.1.1 dfsfeww"],
            [1, "1.2.168.1 skljdf 119r.168.1.1 dfsfeww"],
            [2, "1.2.168.1 sd.1.2.sd 192.168.1.1 dfsfeww"],
        ]
        with self.subTest():
            for num, ip in ip_list:
                result = get_ip(ip)
                self.assertEqual(num, len(result))

    def test_getMulitpleIpWithPort_returnCorrectNumIp(self):
        ip_list = [
            [0, "1sd.2.168.1 skljdf 119r.168.1.1 dfsfeww"],
            [1, "1.2.168.1:80 skljdf 1.2.168.1:8 119r.168.1.1 dfsfeww"],
            [2, "1.2.168.1:8080 sd.1.2.sd 192.168.1.1:32345 dfsfeww"],
        ]
        with self.subTest():
            for num, ip in ip_list:
                result = get_ip_port(ip)
                self.assertEqual(num, len(result))

    def test_wrongStringStructure_returnFalse(self):
        with self.subTest():
            for ip in self.__class__.wrong_ip_list:
                result = is_ip(ip)
                self.assertFalse(result)

    def test_correctStringStructure_returnTrue(self):
        with self.subTest():
            for ip in self.__class__.correct_ip_list:
                result = is_ip(ip)
                self.assertTrue(result)

    def test_numInRange_returnTrue(self):
        num_range = [0, 1, 200, 255]
        with self.subTest():
            for num in num_range:
                result = _num_in_range(num)
            self.assertTrue(result)

    def test_numNotInRange_returnFalse(self):
        num_range = [-1000, -1, 256, 1000]
        with self.subTest():
            for num in num_range:
                result = _num_in_range(num)
            self.assertFalse(result)

    def test_strIpToDec_returnCorrectDecimal(self):
        with self.subTest():
            for src_ip, expected_num in self.__class__.str_decimal_list:
                result = aton(src_ip)
                self.assertEqual(result, expected_num)

    def test_DecToStrIP_returnCorrectString(self):
        with self.subTest():
            for expected_str, src_dec in self.__class__.str_decimal_list:
                result = ntoa(src_dec)
                self.assertEqual(result, expected_str)

    def test_DecToStrIPoutOfRange_raiseValueError(self):
        num_range = [-1, _NTOA_MAX + 1]
        with self.subTest():
            for num in num_range:
                self.assertRaises(ValueError, ntoa, num)

    def test_DecToStrIPwrongType_raiseTypeError(self):
        num = "mock"
        self.assertRaises(TypeError, ntoa, num)

    def test_wrongStringType_raiseTypeError(self):
        num = int()
        self.assertRaises(TypeError, is_ip, num)


if __name__ == "__main__":
    unittest.main()
