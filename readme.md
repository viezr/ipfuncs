# ipfuncs
Module with some useful functions for IPv4 addresses.

## Functions
- `is_ip` function checks if a string is a correct IP address. Any mismatch (e.g. prefix space) returns `False`.
- `get_ip` function returns list of IP addresses found in string. Incorrect addresses are omitted.
- `get_ip_port` function returns list of substrings `<IP addresses>:<port>` found in string. Incorrect addresses are omitted.
- `aton` function returns decimal representation of dotted IP address.
- `ntoa` function returns dotted representation of decimal IP address.
